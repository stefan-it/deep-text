import os
import numpy as np

from keras.layers import Embedding, Conv1D, MaxPooling1D, Flatten, Dense, SpatialDropout1D
from keras.engine import Input
from keras.models import Model, load_model

from gensim.models import KeyedVectors

from util import Util


class Models(object):
    def __init__(self, train_filename,
                 dev_filename,
                 word_embeddings_filename,
                 max_sequence_len=1000,
                 model_name="cnn"):
        """ Constructor for various models for text classification """
        self.model = None
        self.max_sequence_len = max_sequence_len

        self.util = Util(train_filename,
                         dev_filename,
                         word_embeddings_filename,
                         max_sequence_len)

        self.util.construct_embedding_layer()

        if model_name == "cnn":
            self.model = self.__construct_cnn_model__()

    def __construct_cnn_model__(self):
        """ Constructs a Keras model

        In this method we construct a stacked CNN for text classification.

        # Returns
          model: The CNN model
        """
        sequence_input = Input(shape=(self.max_sequence_len,), dtype='int32')
        embedding_layer = self.util.get_embedding_layer()
        embedded_sequences = embedding_layer(sequence_input)
        x = Conv1D(128, 5, activation='relu')(embedded_sequences)
        x = MaxPooling1D(5)(x)
        x = Conv1D(128, 5, activation='relu')(x)
        x = MaxPooling1D(5)(x)
        x = Conv1D(128, 5, activation='relu')(x)
        x = MaxPooling1D(35)(x)
        x = Flatten()(x)
        x = Dense(128, activation='relu')(x)

        preds = Dense(2, activation='sigmoid')(x)

        model = Model(sequence_input, preds)

        return model

    def train(self, batch_size=128,
              epochs=15,
              best_model_filename="best_model.hdf5",
              optimizer='rmsprop',
              tensorboard_log_dir="./log"):
        """ Runs main training

        This method is responsible for the whole training process:

        - Construct the embedding layer
        - Build the Keras model
        - Initialize callbacks for TensorBoard and saving the best model

        After these steps, the Keras model will be compiled and the training
        starts.

        # Arguments
          batch_size         : The batch size used for training
          epochs             : The number of epochs
          best_model_filename: The filename for the best model
          optimizer          : The to be used optimizer
          tensorboard_log_dir: The log directory for TensorBoard
        """
        self.util.construct_tensorboard_callback(tensorboard_log_dir)
        self.util.construct_best_model_callback(best_model_filename)

        self.model.compile(loss='categorical_crossentropy',
                           optimizer=optimizer,
                           metrics=['acc'])

        X_train, y_train = self.util.get_training_data()
        X_dev, y_dev = self.util.get_development_data()

        self.model.fit(X_train, y_train,
                       batch_size=batch_size,
                       epochs=epochs,
                       validation_data=(X_dev, y_dev),
                       callbacks=[self.util.get_tensorboard_callback(),
                                  self.util.get_best_model_callback()])

    def test(self, batch_size=128,
             best_model_filename="best_model.hdf5"):
        """  Runs main evaluation

        This method runs evaluation on the development set. It loads the
        previously saved best model (training) and starts evaluation.

        # Arguments
          batch_size         : The batch size used for evaluation
          best_model_filename: The filename for the best model
        """
        self.best_model = load_model(best_model_filename)

        X_dev, y_dev = self.util.get_development_data()

        scores = self.best_model.evaluate(X_dev,
                                          y_dev,
                                          batch_size=batch_size)
        print("\n%s: %.4f%%" % (self.best_model.metrics_names[1], scores[1]))
