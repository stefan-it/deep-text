# deep-text

`deep-text` is a collection of tools for building deep neural networks for
text classification. State-of-the-art models are implemented for the following
deep learning libraries:

* [*DyNet*:](https://github.com/clab/dynet) The Dynamic Neural Network Toolkit
* [*Keras*](https://github.com/fchollet/keras) - Deep Learning library for Python
  (only *TensorFlow* backend is tested at the moment)
* [*PyTorch*](https://github.com/pytorch/pytorch) - Tensors and Dynamic neural
  networks in Python with strong GPU acceleration

## Data format

The data format for training and development data set must be:

```text
<LABEL> <TAB> <WORDS>
```

So the expected data format uses tabulator as separators. It is mainly inspired
by the [*Kafka-Mann Classification Task*](https://gitlab.com/kafka-mann/task)
dataset [format](https://gitlab.com/kafka-mann/dataset).

## Implementations

Text classification architectures (currenly convolutional neural networks) are
implemented in *DyNet*, *Keras* and *PyTorch*.

There's a simple and homogeneous folder structure for every supported deep
learning toolkit:

`library`:
* `main.py`   - Main method, which includes command line parsing
* `models.py` - Model class, which implements various architectures
* `util.py`   - Utility class, which implements helper methods

# *DyNet*

*DyNet* is a neural network library developed by Carnegie Mellon University and
many others. It is written in C++ (with bindings in Python) and is designed to
be efficient when run on either CPU or GPU, and to work well with networks that
have dynamic structures that change for every training instance.

The *GitHub* repository can be found [here](https://github.com/clab/dynet).
The documentation for compiling and installing all *DyNet* Python binding can
be found [here](http://dynet.readthedocs.io/en/latest/python.html).

## Implementation

The *DyNet* implementation for a text classification system can be found in the
`dynet` folder. The implementation is heavily based on the `nn4nlp2017-code`
code [repository](https://github.com/neubig/nn4nlp2017-code) for the CMU CS
11-747 course [Neural Networks for NLP](http://phontron.com/class/nn4nlp2017/).

## Commandline options

Simply run `python3 dynet/main.py --help` to list all available options:

```mandoc
usage: main.py [-h] [--training-set TRAINING_SET]
               [--development-set DEVELOPMENT_SET] [--model MODEL]
               [--optimizer OPTIMIZER] [--epochs EPOCHS]
               [--embedding-size EMBEDDING_SIZE] [--window-size WINDOW_SIZE]
               [--filter_size FILTER_SIZE] [--model-filename MODEL_FILENAME]
               {train,test}

positional arguments:
  {train,test}

optional arguments:
  -h, --help            show this help message and exit
  --training-set TRAINING_SET
                        Defines training set
  --development-set DEVELOPMENT_SET
                        Defines development set
  --model MODEL         Defines model architecture
  --optimizer OPTIMIZER
                        Defines optimizer used for training
  --epochs EPOCHS       Defines number of training epochs
  --embedding-size EMBEDDING_SIZE
                        Defines embedding size
  --window-size WINDOW_SIZE
                        Defines window size
  --filter_size FILTER_SIZE
                        Defines filter size
  --model-filename MODEL_FILENAME
                        Defines model filename
```

# *Keras*

*Keras* is a high-level neural networks API, written in Python and capable of
running on top of *TensorFlow*, *CNTK*, or *Theano*. It was developed with a
focus on enabling fast experimentation. Being able to go from idea to result
with the least possible delay is key to doing good research.

The GitHub repository can be found [here](https://github.com/fchollet/keras).

Notice: The implementation for text classification is currently only tested with
the *TensorFlow* backend. Therefore use the environment variable
`KERAS_BACKEND=tensorflow`.

## Implementation

The *Keras* implementation for a text classification system can be found in the
`keras` folder.

## Commandline options

Simply run `python3 keras/main.py --help` to list all available options:

```mandoc
usage: main.py [-h] [--training-set TRAINING_SET]
               [--development-set DEVELOPMENT_SET]
               [--word-embeddings WORD_EMBEDDINGS]
               [--max-sequence-len MAX_SEQUENCE_LEN] [--model MODEL]
               [--epochs EPOCHS] [--optimizer OPTIMIZER]
               [--batch-size BATCH_SIZE] [--model-filename MODEL_FILENAME]
               [--tensorboard-logdir TENSORBOARD_LOGDIR]
               {train,test}

positional arguments:
  {train,test}

optional arguments:
  -h, --help            show this help message and exit
  --training-set TRAINING_SET
                        Defines training set
  --development-set DEVELOPMENT_SET
                        Defines development set
  --word-embeddings WORD_EMBEDDINGS
                        Defines filename for Word Embeddings
  --max-sequence-len MAX_SEQUENCE_LEN
                        Defines maximum sequence length
  --model MODEL         Defines model architecture
  --epochs EPOCHS       Defines number of training epochs
  --optimizer OPTIMIZER
                        Defines optimizer used for training
  --batch-size BATCH_SIZE
                        Defines number of batch size
  --model-filename MODEL_FILENAME
                        Defines model filename
  --tensorboard-logdir TENSORBOARD_LOGDIR
                        Defines log directory for TensorBoard
```

# *PyTorch*

*PyTorch* is a Python package that provides two high-level features:

* Tensor computation (like *NumPy*) with strong GPU acceleration
* Deep neural networks built on a tape-based autograd system

The repository can be found [here](https://github.com/pytorch/pytorch).

## Implementation

The *PyTorch* implementation for a text classification system can be found in
the `pytorch` folder.

## Commandline options

Simply run `python3 pytorch/main.py --help` to list all available options:

```mandoc
usage: main.py [-h] [--training-set TRAINING_SET]
               [--development-set DEVELOPMENT_SET] [--model MODEL]
               [--epochs EPOCHS] [--embedding-dim EMBEDDING_DIM]
               [--filters FILTERS] [--filter-sizes FILTER_SIZES]
               [--dropout DROPOUT] [--learning-rate LEARNING_RATE]
               [--batch-size BATCH_SIZE] [--use-cuda]
               [--model-filename MODEL_FILENAME]
               {train,test}

positional arguments:
  {train,test}

optional arguments:
  -h, --help            show this help message and exit
  --training-set TRAINING_SET
                        Defines training set
  --development-set DEVELOPMENT_SET
                        Defines development set
  --model MODEL         Defines model architecture
  --epochs EPOCHS       Defines number of training epochs
  --embedding-dim EMBEDDING_DIM
                        Defines embedding dimension
  --filters FILTERS     Defines number of filters
  --filter-sizes FILTER_SIZES
                        Defines filter sizes
  --dropout DROPOUT     Defines dropout rate
  --learning-rate LEARNING_RATE
                        Defines learning rate
  --batch-size BATCH_SIZE
                        Defines batch size
  --use-cuda            Enables GPU support with CUDA
  --model-filename MODEL_FILENAME
                        Defines model filename
```

# Compatibility matrix

`deep-text` is tested with the following library versions:

| Deep learning library | Version | Python version
| --------------------- | ------- | --------------
| *DyNet*               | *2.0.1* | 3.6.3
| *Keras*               | *2.1.1* | 3.6.3
| *TensorFlow* (Keras)  | *1.3.0* | 3.6.3
| *PyTorch*             | *0.2.0* | 3.6.3

# Contact (Bugs, Feedback, Contribution and more)

For questions about the *deep-text* repository, contact the current maintainer:
Stefan Schweter <stefan@schweter.it>. If you want to contribute to the project
please refer to the [Contributing](CONTRIBUTING.md) guide!

# License

To respect the Free Software Movement and the enormous work of Dr. Richard Stallman
the software in this repository is released under the *GNU Affero General Public License*
in version 3. More information can be found [here](https://www.gnu.org/licenses/licenses.html)
and in `COPYING`.
