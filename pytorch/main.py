import argparse

from models import Models


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("mode", choices=["train", "test"])

    parser.add_argument("--training-set",
                        help="Defines training set")
    parser.add_argument("--development-set",
                        help="Defines development set")
    parser.add_argument("--model", default="cnn",
                        help="Defines model architecture")
    parser.add_argument("--epochs", default=15,
                        help="Defines number of training epochs")
    parser.add_argument("--embedding-dim", default=128,
                        help="Defines embedding dimension")
    parser.add_argument("--filters", default=100,
                        help="Defines number of filters")
    parser.add_argument("--filter-sizes", default="3,4,5",
                        help="Defines filter sizes")
    parser.add_argument("--dropout", default=0.5,
                        help="Defines dropout rate")
    parser.add_argument("--learning-rate", default=0.001,
                        help="Defines learning rate")
    parser.add_argument("--batch-size", default=128,
                        help="Defines batch size")
    parser.add_argument("--use-cuda", action='store_true', default=False,
                        help="Enables GPU support with CUDA")
    parser.add_argument("--model-filename", default='best_model.hdf5',
                        help="Defines model filename")

    args = parser.parse_args()

    if not args.training_set:
        print("Training set is missing!")
        parser.print_help()
        exit(1)

    if not args.development_set:
        print("Development set is missing!")
        parser.print_help()
        exit(1)

    filter_sizes = [int(s) for s in args.filter_sizes.split(",")]

    model = Models(args.training_set,
                   args.development_set,
                   int(args.batch_size),
                   int(args.embedding_dim),
                   int(args.filters),
                   filter_sizes,
                   float(args.dropout),
                   args.model,
                   args.use_cuda)

    if args.mode == "train":
        model.train(epochs=int(args.epochs),
                    learning_rate=float(args.learning_rate),
                    best_model_filename=args.model_filename)

    elif args.mode == "test":
        model.test(best_model_filename=args.model_filename)


if __name__ == '__main__':
    parse_arguments()
