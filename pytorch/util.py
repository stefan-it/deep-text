from torchtext import data
from torchtext import datasets


class Util(object):
    def __init__(self, train_filename,
                 dev_filename,
                 batch_size):
        """ Constructor for our helper class

        This method parses the training and development set

        # Arguments
          train_filename                 : The training set
          dev_filename                   : The development set
        """
        self.train_filename = train_filename
        self.dev_filename = dev_filename
        self.batch_size = batch_size
        self.TEXT = data.Field(lower=True)
        self.LABELS = data.Field(sequential=False)

        self.__parse_datasets__()

    def __parse_datasets__(self):
        """ Parses all data sets

        This method parses training and development set using the TorchText
        module.
        """
        train, dev = data.TabularDataset.splits(
            path='./', train='train', validation='dev', format='tsv', fields=[
                ('label', self.LABELS), ('text', self.TEXT)])

        self.TEXT.build_vocab(train, dev)
        self.LABELS.build_vocab(train, dev)

        self.train_iter, self.dev_iter = data.Iterator.splits((train, dev), batch_sizes=(
            self.batch_size, len(dev)), device=-1, repeat=False, shuffle=True, sort=False)

    def get_training_data(self):
        """ Returns the training data set (as batch iterator)

        # Returns
          self.train_iter: The batch iterator for training data
        """
        return self.train_iter

    def get_development_data(self):
        """ Returns the development data set (as batch iterator)

        # Returns
          self.dev_iter: The batch iterator for development data
        """
        return self.dev_iter

    def get_training_filename(self):
        """ Returns filename of training data

        # Returns
          self.train_filename: Filename of training data
        """
        return self.train_filename

    def get_development_filename(self):
        """ Returns filename of development data

        # Returns
          self.dev_filename: Filename of development data
        """
        return self.dev_filename

    def get_text_field(self):
        """ Returns TorchText text field

        # Returns
          self.TEXT: The TorchText text field
        """
        return self.TEXT

    def get_label_field(self):
        """ Returns TorchText label field

        # Returns
          self.LABELS: The TorchText label field
        """
        return self.LABELS
