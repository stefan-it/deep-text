import torch
import torch.nn as nn
import torch.nn.functional as F


class CNN(nn.Module):
    def __init__(self, embedding_num,
                 embedding_dim,
                 filters,
                 filter_sizes,
                 classes,
                 dropout):
        """ Constructor for CNN

        This implementation of a CNN is heavily based on:

        https://github.com/Shawn1993/cnn-text-classification-pytorch
        """
        super(CNN, self).__init__()

        self.embed = nn.Embedding(embedding_num, embedding_dim)
        self.convs1 = nn.ModuleList([nn.Conv2d(1, filters,
                                               (filter_size, embedding_dim))
                                     for filter_size in filter_sizes])
        self.dropout = nn.Dropout(dropout)
        self.fc1 = nn.Linear(len(filter_sizes) * filters,
                             classes)

    def conv_and_pool(self, x, conv):
        x = F.relu(conv(x)).squeeze(3)
        x = F.max_pool1d(x, x.size(2)).squeeze(2)
        return x

    def forward(self, x):
        x = self.embed(x)
        x = x.unsqueeze(1)
        x = [F.relu(conv(x)).squeeze(3) for conv in self.convs1]
        x = [F.max_pool1d(i, i.size(2)).squeeze(2) for i in x]
        x = torch.cat(x, 1)
        x = self.dropout(x)
        logit = self.fc1(x)
        return logit
