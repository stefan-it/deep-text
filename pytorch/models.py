import sys

import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

from torch.autograd import Variable
from torch.utils.data import DataLoader

from util import Util
from cnnmodel import CNN


class Models(object):
    def __init__(self, train_filename,
                 dev_filename,
                 batch_size,
                 embedding_dim,
                 filters,
                 filter_sizes,
                 dropout,
                 model_name,
                 use_cuda):
        """ Constructor for various models for text classification """
        self.model = None

        self.util = Util(train_filename, dev_filename, batch_size)

        self.use_cuda = use_cuda

        embedding_num = len(self.util.get_text_field().vocab)

        classes = len(self.util.get_label_field().vocab) - 1

        if model_name == "cnn":
            self.model = self.__construct_cnn_model__(embedding_num,
                                                      embedding_dim,
                                                      filters,
                                                      filter_sizes,
                                                      classes,
                                                      dropout)

    def __construct_cnn_model__(self, embedding_num,
                                embedding_dim,
                                filters,
                                filter_sizes,
                                classes,
                                dropout):
        self.model = CNN(embedding_num,
                         embedding_dim,
                         filters,
                         filter_sizes,
                         classes,
                         dropout)

        return self.model

    def __predict__(self, text, model, text_field):
        model.eval()
        text = text_field.preprocess(text)
        text = [[text_field.vocab.stoi[x] for x in text]]
        x = text_field.tensor_type(text)

        if self.use_cuda:
            x = x.cuda()

        x = autograd.Variable(x, volatile=True)

        output = model(x)

        _, predicted = torch.max(output.data, 1)

        return predicted

    def __deep_evaluation__(self, model):
        with open(self.util.get_development_filename(), 'r') as f:
            lines = [line.rstrip() for line in f.readlines()]

        correct = 0

        for line in lines:
            gold_label, text = line.split("\t")

            label = self.__predict__(
                text, model, self.util.get_text_field()).sum()

            gold_label = int(gold_label)

            if label == gold_label:
                correct += 1
        return correct / len(lines)

    def train(
            self,
            epochs,
            learning_rate,
            best_model_filename="best_model.th5",
            use_cuda=False):
        if self.use_cuda:
            self.model.cuda()

        optimizer = torch.optim.Adam(self.model.parameters(), learning_rate)

        steps = 0
        self.model.train()

        best_acc = 0.0

        for epoch in range(1, epochs + 1):
            for batch in self.util.get_training_data():
                feature, target = batch.text, batch.label
                feature.data.t_(), target.data.sub_(1)
                if self.use_cuda:
                    feature, target = feature.cuda(), target.cuda()

                optimizer.zero_grad()
                logit = self.model(feature)

                loss = F.cross_entropy(logit, target)
                loss.backward()
                optimizer.step()

                steps += 1

                corrects = (torch.max(logit, 1)[1].view(target.size()).data
                            == target.data).sum()
                accuracy = corrects / batch.batch_size

                sys.stdout.write(
                    '\rEpoch {} - Batch[{}] - loss: {:.6f}  acc: {:.4f}'.format(epoch, steps,
                                                                                loss.data[0],
                                                                                accuracy))

                if steps % 10 == 0:
                    current_acc = self.__deep_evaluation__(self.model)
                    print("\nAccuracy (test): {:.4f}".format(current_acc))
                    if current_acc > best_acc:
                        best_acc = current_acc
                        print("Saving current model...")
                    torch.save(self.model, best_model_filename)

    def test(self, best_model_filename="best_model.th5"):
        best_model = torch.load(best_model_filename)

        best_acc = self.__deep_evaluation__(best_model)

        print("Accuracy (development): {:.4f}".format(best_acc))
