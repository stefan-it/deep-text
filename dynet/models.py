import random
import dynet as dy
import numpy as np

from util import Util


class Models(object):
    def __init__(self, train_filename,
                 dev_filename,
                 embedding_size,
                 window_size,
                 filter_size,
                 model_name="cnn"):
        """ Constructor for various models for text classification """
        self.embedding_size = embedding_size
        self.window_size = window_size
        self.filter_size = filter_size

        self.model = None

        self.util = Util(train_filename, dev_filename)

        if model_name == "cnn":
            self.model = self.__construct_cnn_model__()

    def __construct_cnn_model__(self):
        model = dy.Model()
        self.embedding_layer = model.add_lookup_parameters(
            (self.util.get_number_words(), 1, 1, self.embedding_size))
        self.cnn_weights = model.add_parameters(
            (1, self.window_size, self.embedding_size, self.filter_size))
        self.cnn_bias = model.add_parameters((self.filter_size))

        self.softmax_weights = model.add_parameters(
            (self.util.get_number_tags(), self.filter_size))
        self.softmax_bias = model.add_parameters((self.util.get_number_tags()))

        return model

    def test(self, words):
        dy.renew_cg()
        cnn_express_weights = dy.parameter(self.cnn_weights)
        cnn_express_bias = dy.parameter(self.cnn_bias)
        softmax_express_weights = dy.parameter(self.softmax_weights)
        softmax_express_bias = dy.parameter(self.softmax_bias)
        if len(words) < self.window_size:
            words += [0] * (self.window_size - len(words))

        cnn_in = dy.concatenate(
            [dy.lookup(self.embedding_layer, x) for x in words], d=1)
        cnn_out = dy.conv2d_bias(
            cnn_in,
            cnn_express_weights,
            cnn_express_bias,
            stride=(
                1,
                1),
            is_valid=False)
        pool_out = dy.max_dim(cnn_out, d=1)
        pool_out = dy.reshape(pool_out, (self.filter_size,))
        pool_out = dy.rectify(pool_out)
        return softmax_express_weights * pool_out + softmax_express_bias

    def __save_model__(self, filename):
        dy.save(filename, [self.embedding_layer,
                           self.cnn_weights, self.cnn_bias])

    def train(self,
              epochs,
              best_model_filename,
              optimizer):
        train = self.util.get_training_data()
        dev = self.util.get_development_data()

        self.trainer = dy.AdamTrainer(self.model)

        best_acc = 0.0

        for epoch in range(1, epochs + 1):
            random.shuffle(train)
            train_loss = 0.0
            train_correct = 0.0

            for tag, words in train:
                scores = self.test(words)
                predict = np.argmax(scores.npvalue())
                if predict == tag:
                    train_correct += 1

                my_loss = dy.pickneglogsoftmax(scores, tag)
                train_loss += my_loss.value()
                my_loss.backward()
                self.trainer.update()
            print("Epoch {} - loss: {:.6f} - acc: {:.4f}".format(epoch,
                                                                 train_loss / len(train), train_correct / len(train)))

            test_correct = 0.0
            for tag, words in dev:
                scores = self.test(words).npvalue()
                predict = np.argmax(scores)
                if predict == tag:
                    test_correct += 1
            print("Accuracy (test): {:.4f}".format(test_correct / len(dev)))

            if test_correct / len(dev) > best_acc:
                print("Save new best model!")
                best_acc = test_correct / len(dev)
                self.__save_model__(best_model_filename)
