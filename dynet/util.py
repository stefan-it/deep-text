from collections import defaultdict


class Util(object):
    def __init__(self, train_filename, dev_filename):
        """ Constructor for our util helper class """
        self.train_filename = train_filename
        self.dev_filename = dev_filename
        self.word_to_idx = defaultdict(lambda: len(self.word_to_idx))
        self.tag_to_idx = defaultdict(lambda: len(self.tag_to_idx))
        self.UNK = self.word_to_idx["<unk>"]

        self.__parse_datasets__()

    def __parse_dataset__(self, filename):
        with open(filename, "r") as f:
            for line in f:
                tag, words = line.lower().strip().split("\t")
                yield (self.tag_to_idx[tag],
                       [self.word_to_idx[x] for x in words.split(" ")])

    def __parse_datasets__(self):
        self.train = list(self.__parse_dataset__(self.train_filename))
        self.word_to_idx = defaultdict(lambda: self.UNK, self.word_to_idx)
        self.dev = list(self.__parse_dataset__(self.dev_filename))
        self.nwords = len(self.word_to_idx)
        self.ntags = len(self.tag_to_idx)

    def get_training_data(self):
        return self.train

    def get_development_data(self):
        return self.dev

    def get_number_words(self):
        return self.nwords

    def get_number_tags(self):
        return self.ntags
