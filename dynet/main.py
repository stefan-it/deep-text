import argparse

from models import Models


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("mode", choices=["train", "test"])

    parser.add_argument("--training-set",
                        help="Defines training set")
    parser.add_argument("--development-set",
                        help="Defines development set")
    parser.add_argument("--model", default="cnn",
                        help="Defines model architecture")
    parser.add_argument("--optimizer", default='rmsprop',
                        help="Defines optimizer used for training")
    parser.add_argument("--epochs", default=15,
                        help="Defines number of training epochs")
    parser.add_argument("--embedding-size", default=128,
                        help="Defines embedding size")
    parser.add_argument("--window-size", default=4,
                        help="Defines window size")
    parser.add_argument("--filter_size", default=128,
                        help="Defines filter size")
    parser.add_argument("--model-filename", default='best_model.dy',
                        help="Defines model filename")
    args = parser.parse_args()

    if not args.training_set:
        print("Training set is missing!")
        parser.print_help()
        exit(1)

    if not args.development_set:
        print("Development set is missing!")
        parser.print_help()
        exit(1)

    model = Models(args.training_set,
                   args.development_set,
                   int(args.embedding_size),
                   int(args.window_size),
                   int(args.filter_size),
                   args.model)

    if args.mode == "train":
        model.train(epochs=int(args.epochs),
                    best_model_filename=args.model_filename,
                    optimizer=args.optimizer)


if __name__ == '__main__':
    parse_arguments()
